#!/bin/bash
# This runs the bftpd daemon, which must be configured properly first

# interface to run on
FTP_IF=wlan1

get_ip() {
	local if=$1
	local ip=$(sudo ifconfig ${if} | awk '/\<inet\>/{print $2}')
	printf "%s" $ip
}

run_ftpd() {
	local if=$1
	local ip=$(get_ip $if)
	sudo sed "s/.*BIND_TO_ADDR.*/	BIND_TO_ADDR=\"${ip}\"/g" -i ~/../usr/etc/bftpd.conf
	printf "Running ftpd @ %s\n" $ip
	sudo bftpd -Dc ~/../usr/etc/bftpd.conf
}
run_ftpd $FTP_IF
