#!/bin/bash

source ~/func-table.bash

# This is the main function, and all it basically does is toggles an app on/off (enable/disable)
func_TOGGLE() {
	local input=$1
	local appname=$(func_table "ns" $input)
	local state=$(sudo pm list packages -d | grep "$appname" && echo disabled || echo enabled)
 	[ $(sudo pm list packages -d | grep "$appname") ] && sudo pm enable "$appname" || sudo pm disable "$appname"
}

stat_ns() {
	local input=$1
	local appname=$(func_table "ns" $input)

	sudo pm list packages -e | grep "$appname" 1>>/dev/null && printf "enabled" || printf "disabled"
}

toggles() {
	local state
	local count
	# total entries in func_table
	local entries=FUNC_TABLE_SZ

	printf "Main Toggle Menu\n"
	for ((count=1; count<=$entries; count++)) {
		printf "%s) %s (%s)\n" $count "$(func_table "name" $count)" "$(stat_ns $count)"
	}
	printf "q) Quit\n"
	printf "\n> "
	read input

	if [[ "$input" == "q" ]]; then
		return
	fi
	func_TOGGLE "$input"
}

input="$1"
toggles
