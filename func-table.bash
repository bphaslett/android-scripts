
FUNC_TABLE_SZ=3
# hash table that is called with two pieces of input, 1) column name to query, 2) entry number in the respective column
# eg:
#   func_table "name" 4
#   func_table "ns" 10
# 
func_table() {
	local column="$1"
	local input="$2"

	if [[ "$column" == "name" ]]; then
		case $input in
			"1")
				printf "Play Store"
				;;
			"2")
				printf "Google Home"
				;;
			"3")
				printf "Google Drive"
				;;
		esac
	elif [[ "$column" == "ns" ]]; then
		case $input in
			"1")
				printf "com.android.vending"
				;;
			"2")
				printf "com.google.android.apps.chromecast.app"
				;;
			"3")
				printf "com.google.android.apps.docs"
				;;
				;;
		esac
	fi
}


