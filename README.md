# Android scripts

This is just a collection of bash functions and scripts I wrote to help automate stuff on my Android device.  The toggles menu is the one I'm really proud of, partly because of the makeshift function table I created to help make adding entries easier. It works similarly to an enumeration in C or a hash table in Perl.  Entries in the namespace table should match up with descriptions in the other, and the total number of entries is contained in a global.
