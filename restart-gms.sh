#!/bin/sh -e
# dumpsys activity services showed casting services
# tied into this package

for i in com.google.android.gms; do
  pm disable "$i"
	sleep 2
	pm enable "$i"
done
# pm disable com.google.android.gms
# pm enable com.google.android.gms

# pm disable com.qualcomm.atfwd
# pm enable com.qualcomm.atfwd

# pm disable com.google.android.networkstack.tethering
# pm enable com.google.android.networkstack.tethering

# pm disable com.android.providers.media
# pm enable com.android.providers.media

# pm disable com.google.android.providers.media.module
# pm enable com.google.android.providers.media.module


