#!/bin/bash

source ~/func-table.bash

stat_PLAY() {
	local appname=$(func_table ns 1)
	sudo pm list packages -e | grep -q "$appname" && printf "enabled" || printf "disabled"
}

play_daemon() {
	local sleep_time=10
	local appname=$(func_table ns 1)
	local status

	echo "Toggles daemon active for "$appname"."
	while :; do
		status=$(stat_PLAY)
		if [[ "$status" == "enabled" ]]; then
			printf "app %s is on at %s, disabling..\n" $appname $(date +%H:%M)
			sudo pm disable "$appname"
		fi
		sleep "$sleep_time"
	done
}

play_daemon
