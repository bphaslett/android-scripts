alias lsh='ls -ladh'
alias mdl='megadl'
alias pss="sudo ps -A -o pid,%mem,%cpu,args"

download_album() {
  local program=yt-dlp
  if [[ -n $1 ]]; then
    local album=$1
  else
    printf "${FUNCNAME[0]} [address] [tracks (1-3,5-7,12)] (optional)"
    return
  fi
  if [[ -n $2 ]]; then
    local tracks=$2
    "$program" -x --audio-format mp3 --audio-quality 0 --playlist-items "$tracks" -o "%(playlist_index)02d - %(title)s.%(ext)s" "$album"
  else
    "$program" -x --audio-format mp3 --audio-quality 0 -o "%(playlist_index)02d - %(title)s.%(ext)s" "$album"
  fi
}
pull_songlist () {
  local program=yt-dlp
	local count=0
	local file=$1
	for i in $(cat "$file"); do
		((count++))
		"$program" -x --audio-format mp3 --audio-quality 0 -o "$(printf "%02d" $count) - %(title)s.%(ext)s" "$i"
	done
}

make_mp3 () {
	local fn="$1"
	local bn=$(echo $fn | cut -d. -f1)
	local ext=$(echo $fn | cut -d. -f2)
	ffmpeg -i "$bn"."$ext" -ar 44100 -ac 2 -b:a 192k "$bn".mp3
}
cgrep() {
	local input=$1
	find -type f -name '*.[chS]' -exec grep -Hn "$input" {} \;
}

# for seeing if something in chroot is still installed (gentoo chroot)
check_pkg () {
        local pkg=$1
        for i in $(tar tzf "$pkg" | sed 's/\(.*\)/\/\1/g'); do
                [[ -e "$i" ]] && echo "$i"
        done
}

test_proxy() {
        local fn=$1
        local url=$2
        for i in $(cat $fn); do
                echo "testing $i"
                curl --connect-timeout 2 -x https://${i} ${url}
        done
}

stat_PLAY() {
	local appname=$(func_table ns 1)
	sudo pm list packages -e | grep -q "$appname" && printf "enabled" || printf "disabled"
}

play_daemon() {
	local sleep_time=10
	local appname=$(func_table ns 1)
	local status

	echo "Toggles daemon active for "$appname"."
	while :; do
		status=$(stat_PLAY)
		if [[ "$status" == "enabled" ]]; then
			printf "app %s is on at %s, disabling..\n" $appname $(date +%H:%M)
			sudo pm disable "$appname"
		fi
		sleep "$sleep_time"
	done
}

# this takes a letterboxd csv as input, strips out the addresses, then grabs the director name, then you can pipe the result to sort and uniq -c
get_directors() {
	local file=$1
	local addr
	for i in $(cat $file | sed 's/\r/\n/g'); do
		addr=$(printf "$i" | sed 's/.*\(https[^,]*\),.*/\1/g')
		curl -s -L "$addr" | grep "Directed by.*content" | sed -e 's/.*content=\"\([^\"]*\)\"/\1/g' -e 's/ \/>//g'
	done
}

build_stream() {
	local infile=$1
	local outfile=$2
#	ffmpeg -i "$infile" -c copy "$outfile"
	ffmpeg -allowed_extensions ALL -i "$infile" -c copy "$outfile"
}

# for tagging mp3s, everything else musicolet can handle
write_tags() {
	local file
	local input
	local artist
	local album
	local year
	local genre

	printf "Mass tag (y/n)? "
	read input
	if [[ "$input" == "y" ]]; then
		printf "Artist: "
		read artist
		printf  "Album: "
		read album
		printf "Genre: "
		read genre
		printf "Year: "
		read year

		for file in *.mp3; do
			echo id3ted -t $(echo $file | sed 's/.* - \(.*\)\.mp3/\"\1\"/g') -T $(echo $file | sed 's/\(.*\) - .*/\"\1\"/g') -a \"$artist\" -A \"$album\" -g \"$genre\" -y "$year" \"$file\" | bash
		done

	else

		for file in *.mp3; do
			echo id3ted -t $(echo $file | sed 's/.* - \(.*\)\.mp3/\"\1\"/g') -T $(echo $file | sed 's/\(.*\) - .*/\"\1\"/g') \"$file\" | bash
		done

	fi
}

# for .m3u8 streams without a playlist
concat_playlist() {
	cat $(ls -1 *.jpg | sort -t_ -nk3|tr '\n' ' ')  > converted.ts
}
build_stream2() {
  if [[ -z $1 ]]; then
		printf "${FUNCNAME[0]} [output file] (required)\n"
		return
	fi
	local outfile=$1
	concat_playlist
	ffmpeg -i converted.ts -c copy "$outfile"

}
